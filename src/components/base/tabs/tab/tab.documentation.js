import examples from './examples';
import description from './tab.md';

export default {
  description,
  examples,
  bootstrapComponent: 'b-tab',
  followsDesignSystem: true,
};
